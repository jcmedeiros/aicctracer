## Introduction
This is a very simple Python script that allows a user to simulate the first interactions between two LMS, when a AICC course is launched.
## How to use
Download the `AICCTracerCourse.zip` file available in the [downloads page](https://bitbucket.org/jcmedeiros/aicctracer/downloads/).
Then, upload it into the LMS you want to inspect. Once the course is available launch it.

The first page shows the AICC parameters that were provided in the launch request and provides a link, `GetParam`, used to issue 
the AICC `GetParam` command, which is the first command the provider LMS have to issue.

Click on it and you'll get a similar page showing the way the command was issued and the details of the response.

At this point, the user can send `GetParam` or `PutParam` commands. When issuing a `PutParam` the user 
can specify a completion status and a score value. Once the command is executed, the details of the operation are displyed.
After that each `PutParam`, click in GetParam and check if the LMS has updated the status and score accordingly.

