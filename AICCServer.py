from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import os
import urllib
import urllib2
import re
import uuid
import urlparse
import httplib

session_map = {}

#Create custom HTTPRequestHandler class
# https://www.junian.net/python-http-server-client/
class AICCRequestHandler(BaseHTTPRequestHandler):

    #handle GET command
    def do_GET(self):
        return self._handle_request()

    #handle POST command
    def do_POST(self):
        return self._handle_request()

    #handle any request
    def _handle_request(self):
        try:
            response = self._get_contents()

            #send code 200 response
            self.send_response(200)

            #send header first
            self.send_header('Content-type','text-html')
            self.end_headers()

            #send file content to client
            self.wfile.write(response)
            return
        except InvalidRequestException:
            self.send_error(404)
        except AssertionError as error:
            print(error)


    def _get_contents(self):
        if (self._is_launching()):
            contents = self._render_first_page(self._create_session())
        elif (self._is_get_param()):
            session = self._lookup_session()
            req_draft, response = session.send_get_param_command()
            contents = self._render_general_pages(session, req_draft, response)
        elif (self._is_put_param()):
            session = self._lookup_session()
            status, score = self._parse_body()
            req_draft, response  = session.send_put_param_command(status, score)
            contents = self._render_general_pages(session, req_draft, response)
        elif (self._is_exitau()):
            session = self._lookup_session()
            req_draft, response = session.send_exitau_command()
            contents = self._render_general_pages(session, req_draft, response)
        else:
            raise InvalidRequestException(self.path)

        return contents


    def _is_launching(self):
        return self.path.startswith('/launch')

    def _is_get_param(self):
        return self.path.startswith('/get_param')

    def _is_put_param(self):
        return self.path.startswith('/put_param')

    def _is_exitau(self):
        return self.path.startswith('/exit_au')

    def _extract_launch_details(self):
        session = re.search('aicc_sid=([^&]+)', self.path, re.IGNORECASE)
        url = re.search('aicc_url=([^&]+)', self.path, re.IGNORECASE)
        return (session.group(1) if session else '', url.group(1) if url else '')

    def _create_session(self):
        lms_session, lms_end_point = self._extract_launch_details()
        sid = str(uuid.uuid1())
        session_map[sid] = AiccSession(sid, urllib.unquote(lms_session), urllib.unquote(lms_end_point))
        return session_map[sid]

    def _lookup_session(self):
        session_id = self._extract_session_id()
        return session_map[session_id]

    def _extract_session_id(self):
        session_id = re.search('sid=([^&]+)', self.path, re.IGNORECASE)
        return session_id.group(1) if session_id else ''

    def _parse_body(self):
        length = int(self.headers.getheader('content-length'))
        field_data = self.rfile.read(length)
        fields = urlparse.parse_qs(field_data)
        status = fields['status'][0] if 'status' in fields else ''
        score = fields['score'][0] if 'score' in fields else ''
        return (status, score)

    def _render_first_page(self, session):
        return self._html_template % (session.aicc_session, session.aicc_url, session.session_id, session.session_id, '')

    def _render_general_pages(self, session, request, response):
        remaining = self._html_commands_form % (session.session_id, request, response)
        return self._html_template % (session.aicc_session, session.aicc_url, session.session_id, session.session_id, remaining)


    _html_template = """\
        <!DOCTYPE html>
        <html lang="en"><head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <title>AICC Tracer</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
            </head>
        <body class="container">
        <div class="row"><div class="col-md-12">
        <h2>AICC tracing</h2>
        <p>The hosting LMS, that launched this page, has defined the following parameters for communication:</p>
        <table class="table table-sm">
          <tr><th scope="row">SessionId</th><td>%s</td></tr>
          <tr><th scope="row">AICC URL</th><td>%s</td></tr>
        </table>
        <h3 class="mt-5">Available Commands</h3>
        <p><a class="btn btn-outline-primary" href="/get_param?sid=%s">GetParam</a></p>
        <p><a class="btn btn-outline-primary" href="/exit_au?sid=%s">ExitAU</a></p>
        %s
        </div></div></body></html>
        """

    _html_commands_form = """\
        <form method="POST" action="/put_param?sid=%s">
          <input class="btn btn-outline-primary" type="submit" value="PutParam" />
          <label for="status">Satus</label>
          <select name="status">
            <option selected value="">Select a status</option>
            <option value="passed">Passed</option>
            <option value="failed">Failed</option>
            <option value="complete">Completed</option>
            <option value="incomplete">Incomplete</option>
            <option value="browsed">Browsed</option>
            <option value="not attempted">Not Attempted</option>
          </select>
          <label for="score">Score</label>
          <input type="text" name="score" value="0" />
        </form>
        <h3 class="mt-5">Command Draft</h3>
        <pre>%s</pre>
        <h3 class="mt-5">Command Response</h3>
        <pre>%s</pre>
        """

class AiccSession:
    def __init__(self, session_id, aicc_session, aicc_url):
        self.session_id = session_id
        self.aicc_session = aicc_session
        self.aicc_url = aicc_url

    def send_get_param_command(self):
        return self._send_command('getparam')

    def send_put_param_command(self, status, score):
        aicc_data = """[Core]\r\n Lesson_Location = 97\r\n Lesson_Status = %s \r\n Score = %s\r\n Time = 00:01:00\r\n[Core_Lesson]""" % (status, score)
        return self._send_command('putparam', aicc_data)

    def send_exitau_command(self):
        return self._send_command('exitau')

    def _send_command(self, command, aicc_data = ''):
        data = """command=%s&version=3.5&session_id=%s""" % (command, self.aicc_session)
        prettyPrintData = data
        if (aicc_data):
            prettyPrintData = data + "&aicc_data=" + aicc_data
            data = data + "&aicc_data=" + urllib.quote(aicc_data)

        print data
        request_data = """POST %s\n\n%s""" % (self.aicc_url, prettyPrintData)

        request = urllib2.Request(self.aicc_url, data)
        request.add_header("Content-Type",'application/x-www-form-urlencoded')
        request.get_method = lambda: "POST"
        response = urllib2.urlopen(request)
        contents = """%s\n%s""" % (response.info(), response.read())
        return (request_data, contents)


class InvalidRequestException(Exception):
    pass


def run():
    print('http server is starting...')

    #ip and port of servr
    #by default http server port is 80
    server_address = ('', int(os.environ.get('PORT', 8888)))
    httpd = HTTPServer(server_address, AICCRequestHandler)
    print('http server is running...')
    httpd.serve_forever()

if __name__ == '__main__':
    run()

